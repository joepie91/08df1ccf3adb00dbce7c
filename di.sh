VLC_BIN=`which vlc`
CHANNEL="/di_trance_aac"

curl "http://pub5.di.fm$CHANNEL" \
    -H 'Accept: */*' \
    -H 'Connection: keep-alive' \
    -H 'Referer: http://www.di.fm/' \
    -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36' \ -H 'Accept-Language: en-US,en;q=0.8' \
    --compressed | $VLC_BIN -
